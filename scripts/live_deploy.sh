clear
echo "[ DEPLOY START ]"
yarn build --prod --configuration=production &&
aws s3 sync ./dist s3://ica2021.cf-air.com --acl public-read &&
aws s3 sync ./dist s3://online.icaworldcoopcongress.coop/ --acl public-read
echo "[ DEPLOY DONE ]"