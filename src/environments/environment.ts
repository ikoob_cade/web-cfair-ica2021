export const environment = {
  production: false,
  // base_url: 'http://192.168.1.11:8038/api/v1', // localhost
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  eventId: '61135c2d64ca7d00121a7854' // ICA 2021
};
