export const environment = {
  production: false,
  base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1', // 개발서버,
  socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',
  eventId: '6136c1212cdb1a001365e42c' // ICA 2021
};
