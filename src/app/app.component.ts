import * as _ from 'lodash';
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SocketService } from './services/socket/socket.service';
import { DataService } from './services/data.service';
import { DatePipe } from '@angular/common';
import { HistoryService } from './services/api/history.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { MemberService } from './services/api/member.service';
import { TranslateService } from '@ngx-translate/core';
import { FunctionService } from './services/function/function.service';

declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  @ViewChild('NetworkInput') networkInput: ElementRef;
  public subDomain: string;
  public isBoard = false;
  public isIe = false;
  public messageIndex = 0;
  public user;

  public sampleData: any = {
    A: ['item', 'item', 'item', 'item'],
    B: ['item', 'item', 'item', 'item'],
  };

  /** Network */
  public isShowNetwork: boolean;
  public search: FormGroup;
  public memberList = [];
  public memberListLoaded = true;
  public isLoading = null;
  public selector = '#NetworkContainer';



  private originMembers = [];
  private total = 0;
  public formattedTotal = '';
  public page = 1;
  public mySessionStorage = sessionStorage;
  private lastSubmit = '';
  private searchedTotal = 0;
  public formattedSearchedTotal = '';
  curLang;
  subscriptions = [];

  constructor(
    private router: Router,
    private historyService: HistoryService,
    private deviceService: DeviceDetectorService,
    private socketService: SocketService,
    private dataService: DataService,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private memberService: MemberService,
    private translateService: TranslateService,
    private functionService: FunctionService,
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);

    this.router.events
      .pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd))
      .subscribe((event) => {
        this.isBoard = (event.url === '/board' || event.url === '/board-regist');
        // * 페이지 이동시 열려있는 모달 모두 닫기
        $('.modal').modal('hide');
      });

    // 소켓 초기화
    this.socketService.init();
  }

  ngOnInit(): void {
    this.search = this.fb.group({
      keyword: ['']
    });

    const device = this.deviceService.getDeviceInfo();
    if (device.browser === 'IE') {
      this.isIe = true;
      this.router.navigate(['/not-ie']);
    }

    // 토스트 데이터 수신 구독
    this.dataService.currentReceiveMessage
      .subscribe((data) => {
        if (!data) {
          return;
        }

        if (data.roomId) {
          const room = this.historyService.getRoom();
          if (!room) {
            return;
          }
          if (room.id !== data.roomId) {
            return;
          }
        }

        const date = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm');

        let description = data.description;
        if (data.descriptionMultilingual) {
          description = JSON.parse(data.descriptionMultilingual)[this.curLang];
        }

        const innerHTML = `
      <div class="toast-header">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle-fill" viewBox="0 0 16 16">
      <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
      </svg>
        <strong class="mr-auto">&nbsp;Notification</strong>
        <small>${date}</small>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="toast-body">
      <b>${data.title}</b><br>
        ${description}
      </div>`;

        const toastElement = document.createElement('div');
        toastElement.setAttribute('id', `notiToast-${this.messageIndex}`);
        toastElement.setAttribute('role', 'alert');
        toastElement.setAttribute('aria-live', 'assertive');
        toastElement.setAttribute('aria-atomic', 'true');
        toastElement.setAttribute('class', 'toast');
        toastElement.setAttribute('data-autohide', 'false');
        toastElement.innerHTML = innerHTML;
        $('.toast-wrapper').append(toastElement);
        $(`#notiToast-${this.messageIndex}`).toast('show');
        this.messageIndex++;
      });
  }

  openKakao(): void {
    window.open('http://pf.kakao.com/_rxnSfs/chat', '_blank');
  }

  // ! API 회원목록 조회
  getMemberList(): void {
    this.memberService.findAll().subscribe(res => {
      this.originMembers = _.sortBy(res, 'name');
      this.total = this.originMembers.length;
      this.formattedTotal = this.functionService.padStart(this.total, 3);
      this.getFilteredMembers();
    });
  }

  searchedOriginMembers = [];

  // ! LastName 알파벳으로 재구성
  getFilteredMembers(isSearch?: boolean): void {
    try {
      let members;
      if (isSearch && this.lastSubmit) {
        members = this.originMembers;
      } else {
        members = this.originMembers.slice(0, this.page * 10);
      }
      const memberList = [];
      let searchedTotal = 0;
      for (let i = 65; i <= 90; i++) {
        const alphabet = String.fromCharCode(i);

        const filteredMembers = _.filter(members, member => {
          const compareName: boolean = member.name.toUpperCase().startsWith(alphabet);

          let compareSearch = true;
          if (this.lastSubmit) {
            const searchText = this.lastSubmit.toLowerCase();
            compareSearch
              = member.name.toLowerCase().includes(searchText) ||
              (member.firstName ? member.firstName.toLowerCase().includes(searchText) : false) ||
              (member.lastName ? member.lastName.toLowerCase().includes(searchText) : false);
          }

          return compareName && compareSearch;
        });

        memberList.push({ header: alphabet, filteredMembers });
        searchedTotal += filteredMembers.length;
        this.isShowNetwork = true;
      }

      this.searchedTotal = searchedTotal;
      this.formattedSearchedTotal = this.functionService.padStart(this.searchedTotal, 3);

      this.memberList = memberList;
      this.spinner.hide();
      this.isLoading = null;
    } catch (err) {
      this.spinner.hide();
    }
  }

  showNetwork(event): void {
    // 이미 열려있는 경우 닫기
    if (!event || this.isShowNetwork) {
      this.isShowNetwork = false;
      return;
    }

    this.user = JSON.parse(this.mySessionStorage.getItem('cfair')); // 내 정보
    this.search.controls.keyword.patchValue(''); // 검색필드 초기화
    this.lastSubmit = ''; // 검색값 초기화

    /**
     * 행사일기준으로, 사용자목록이 존재하는 가정하에 개발함.
     * 컴포넌트에서 가지고있는 원본목록이 없을 경우에만 API Call...
     * @author jinhyuc.ko
     */
    if (!this.originMembers.length && this.user) {
      this.getMemberList();
    } else {
      this.getFilteredMembers();
    }
  }

  hideNetwork(): void {
    this.isShowNetwork = false;
  }

  isShowCollapse(selector): boolean {
    const el = $(selector);
    if (el[0]) {
      return el.css('display') !== 'none';
    } else {
      return false;
    }
  }

  mailto(email): void {
    window.open(`mailto:${email}`, '_blank');
  }

  // ! Network - 검색
  submit(): void {
    if (this.memberListLoaded) {
      this.lastSubmit = this.search.controls.keyword.value;
      this.isLoading = true;
      this.spinner.show();
      this.memberListLoaded = false;
      this.searchedTotal = 0;

      setTimeout(() => {
        this.memberListLoaded = true;
        this.getFilteredMembers(true);
      }, 0);
    }

    // this.memberService.findAll().subscribe(res => {
    //   this.memberList = res;
    //   this.spinner.hide();
    // });
  }

  // ! Network - Infinite Scroll
  onScroll(): void {
    if (this.page * 10 < this.total) {
      this.spinner.show();

      setTimeout(() => {
        this.page++;
        this.memberList = [];
        this.getFilteredMembers();
      }, 500);
    }
  }

  resetNetwork(): void {
    if (this.lastSubmit) {
      this.search.controls.keyword.patchValue(''); // 입력값 초기화
      this.lastSubmit = this.search.controls.keyword.value;
      // this.networkInput.nativeElement.focus();
      this.getFilteredMembers();
    }
  }

  goTop(): void {
    window.scrollTo({ top: 0 });
  }
}
