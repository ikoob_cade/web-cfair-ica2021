import * as _ from 'lodash';
import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { CategoryService } from '../../services/api/category.service';
import { SpeakerService } from '../../services/api/speaker.service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';

declare let $: any;
@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit, OnDestroy {
  public speakers: Array<any> = [];

  allParentCategorys = [
    { categoryName: 'Main Congress<br>(12/1~12/3)', dataValue: 'main congress' },
    { categoryName: 'Pre-Congress<br>(11/28~11/30)', dataValue: 'pre congress' },
  ];

  curParentCategory = 0;

  public speakerCategories: Array<any> = [];
  public speakerDetail;
  public speakerLoaded = false;

  public user = sessionStorage.getItem('cfair');

  public curLang;
  private subscriptions = [];

  constructor(
    private speakerService: SpeakerService,
    private categoryService: CategoryService,
    private router: Router,
    private dataService: DataService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  ngOnInit(): void {
    // this.loadCategories();
    this.loadSpeakers();
  }
  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  originSpeakers;

  // 발표자 리스트를 조회한다.
  loadSpeakers = () => {
    this.speakerService.find(false).subscribe(res => {
      res = _.sortBy(res, 'fullName');
      this.originSpeakers = res;
      this.speakers = _.filter(res, speaker => {
        return speaker.depart && speaker.depart.indexOf(this.allParentCategorys[this.curParentCategory].dataValue) >= 0;
      });
    });
  }

  setParentCategory(i): void {
    this.curParentCategory = i;
    this.speakers = _.filter(this.originSpeakers, speaker => {
      return speaker.depart && speaker.depart.indexOf(this.allParentCategorys[this.curParentCategory].dataValue) >= 0;
    });
  }

  originCategories;

  loadCategories = () => {
    this.categoryService.find('speaker').subscribe(res => {
      this.originCategories = res;

      _.forEach(res, category => {
        category.speakers = _.sortBy(category.speakers, 'seq');
      });

      _.forEach(res, category => {
        category.speakers = _.filter(this.speakers, speaker => speaker.categoryIds && speaker.categoryIds.includes(category.id));
      });

      this.speakerCategories = res;
    });
  }

  getDetail(speaker): void {
    setTimeout(() => {
      this.speakerLoaded = false;
    }, 0);

    if (this.user) {
      this.speakerService.findOne(speaker.id).subscribe(res => {
        this.speakerDetail = res;
        this.speakerLoaded = true;
        $('#speakersModal').modal('show');
      });
    } else {
      this.router.navigate(['/login']);
    }
  }
}
