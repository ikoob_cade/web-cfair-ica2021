import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BoardService } from '../../../services/api/board.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../../services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit {

  public board: any;
  public user: any;

  public formGroup: FormGroup;
  public isEditMode: boolean = false;

  private listPageSate;
  curLang;

  subscriptions = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private boardService: BoardService,
    private fb: FormBuilder,
    private dataService: DataService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);

    if (sessionStorage.getItem('cfair')) {
      this.user = JSON.parse(sessionStorage.getItem('cfair'));
    }
    this.formGroup = this.fb.group({
      password: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')])],
    });

    const navigation = this.router.getCurrentNavigation();
    if (navigation.extras?.state) {
      const { myPostingsState, curPage, keywordSelect, keyword } = navigation.extras.state;

      this.listPageSate = { myPostingsState, curPage, keywordSelect, keyword };
    }
  }

  ngOnInit(): void {
    this.getBoardById(this.route.snapshot.params.boardId);
  }

  ngOnDestroy() {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  getBoardById(boardId: string): void {
    this.boardService.findOne(boardId)
      .subscribe((resp: any) => {
        this.board = resp;
      });
  }

  // 이메일 / 비번 확인
  submit(): void {
    this.isEditMode = false;
    if ((this.formGroup.value.email !== this.board.email) || (this.formGroup.value.password.toString() !== this.board.password)) {
      return alert('글 작성시 입력한 이메일 또는 비밀번호를 확인해주세요.');
    } else {
      this.isEditMode = true;
    }
  }

  // 게시글 삭제하기
  delete(): void {
    const result = confirm('Are you sure you want to delete?');
    if (result) {
      this.boardService.delete(this.board.id).subscribe((resp: any) => {
        alert('Deleted.');
        this.router.navigate(['/board'], { replaceUrl: true });
      });
    }
  }

  /**
   * 뒤로가기
   * 상세페이지 진입 시 내가쓴글 보기 여부, 당시 페이지번호를 돌려준다.
   */
  backToList(): void {
    const state = { ...this.listPageSate };
    this.router.navigateByUrl(
      '/board', {
      state,
      replaceUrl: false
    });
  }

}
