import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BoardService } from '../../../services/api/board.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DocumentService } from '../../../services/api/document.service';
declare let $: any;
@Component({
  selector: 'app-document-write',
  templateUrl: './document-write.component.html',
  styleUrls: ['./document-write.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DocumentWriteComponent implements OnInit {
  pageTitle = '';
  public user: any;
  public formGroup: FormGroup;
  public board: any;
  categoryId;
  categoryName;
  file;

  documentId;
  document;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private boardService: BoardService,
    private documentService: DocumentService
  ) { }

  ngOnInit(): void {
    this.setFormGroup();
    this.route.queryParams.subscribe(res => {
      this.categoryId = res.categoryId;
      this.categoryName = res.categoryName;
      if (res.documentId) {
        this.pageTitle = 'Update';
        this.documentId = res.documentId;
        this.getDocument();
      } else {
        this.pageTitle = 'Create';
      }
    });
  }

  setFormGroup(): void {
    this.formGroup = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      titleEn: ['', Validators.compose([Validators.required])],
      titleEs: ['', Validators.compose([Validators.required])],
      titleFr: ['', Validators.compose([Validators.required])],
      titleKo: ['', Validators.compose([Validators.required])],
      isFixed: [false],
      author: ['', Validators.compose([Validators.required])],
    });
  }

  getDocument(): void {
    this.documentService.findOne(this.documentId).subscribe(res => {
      this.document = res;
      this.formGroup.setValue({
        title: this.document.title,
        titleEn: this.document.titleMultilingual.en,
        titleEs: this.document.titleMultilingual.es,
        titleFr: this.document.titleMultilingual.fr,
        titleKo: this.document.titleMultilingual.ko,
        isFixed: this.document.isFixed,
        author: this.document.author
      });
    });
  }

  // 글 작성 완료
  submit(): void {
    this.formGroup.controls.title.setValue(this.formGroup.controls.titleEn.value);
    if (this.formGroup.invalid || (!this.document && !this.file)) {
      let target: string;
      switch (true) {
        case this.formGroup.controls.titleEn.invalid:
          target = 'Session Name(English)';
          break;
        case this.formGroup.controls.titleEs.invalid:
          target = 'Session Name(Español)';
          break;
        case this.formGroup.controls.titleFr.invalid:
          target = 'Session Name(Français)';
          break;
        case this.formGroup.controls.titleKo.invalid:
          target = 'Session Name(Korean)';
          break;
        case this.formGroup.controls.author.invalid:
          target = 'Author';
          break;
        case !this.file:
          target = 'Attachment';
          break;
        default:
          break;
      }
      return alert(`Input ${target}, please.`);
    } else {
      this.document ? this.update() : this.create();
    }
  }

  onChange($event): void {
    this.file = $event.target.files[0];
  }

  removeFile(): void {
    const attachmentElement: any = document.getElementById('attachment');
    attachmentElement.value = '';
    this.file = null;
  }

  // 글 추가하기
  create(): any {
    const titleMultilingual = {
      en: this.formGroup.controls.titleEn.value,
      es: this.formGroup.controls.titleEs.value,
      fr: this.formGroup.controls.titleFr.value,
      ko: this.formGroup.controls.titleKo.value
    };

    const payload = new FormData();
    payload.append('title', this.formGroup.controls.title.value);
    payload.append('titleMultilingual', JSON.stringify(titleMultilingual));
    payload.append('categoryId', this.categoryId);
    payload.append('isFixed', this.formGroup.controls.isFixed.value);
    payload.append('file', this.file);
    payload.append('author', this.formGroup.controls.author.value);

    this.documentService.create(payload)
      .subscribe((resp: any) => {
        alert('created.');
        this.router.navigate(['/documents'], { queryParams: { categoryId: this.categoryId } });
      });
  }

  update(): void {
    const titleMultilingual = {
      en: this.formGroup.controls.titleEn.value,
      es: this.formGroup.controls.titleEs.value,
      fr: this.formGroup.controls.titleFr.value,
      ko: this.formGroup.controls.titleKo.value
    };

    const payload = new FormData();
    payload.append('title', this.formGroup.controls.title.value);
    payload.append('titleMultilingual', JSON.stringify(titleMultilingual));
    payload.append('categoryId', this.categoryId);
    payload.append('isFixed', this.formGroup.controls.isFixed.value);
    if (this.file) {
      payload.append('file', this.file);
    }
    payload.append('author', this.formGroup.controls.author.value);

    this.documentService.update(this.documentId, payload)
      .subscribe((resp: any) => {
        alert('updated.');
        this.router.navigate(['/documents'], { queryParams: { categoryId: this.categoryId } });
      });
  }

  // 글 작성 취소
  cancelWrite(): void {
    const result = confirm('Are you sure you wish to cancel?');
    if (result) {
      this.router.navigate(['/bloard']);
    }
  }

}
