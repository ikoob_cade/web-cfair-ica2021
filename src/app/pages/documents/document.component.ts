import * as _ from 'lodash';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { DocumentService } from '../../services/api/document.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FunctionService } from '../../services/function/function.service';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';
import { CategoryService } from '../../services/api/category.service';
import { RoleService } from '../../services/roleService.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit, OnDestroy {
  public allDocuments: Array<any>; // 전체 포스터 목록
  public allCategories: Array<any>; // 전체 카테고리 목록
  public documents: Array<any>; // 포스터 목록
  public searchText: string; // 검색어
  public searchType: string; // 검색 카테고리
  public user: any = sessionStorage.getItem('cfair');

  curCategory = 0;
  curLang;
  private subscriptions = [];
  roles: any = {};


  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private documentService: DocumentService,
    private functionService: FunctionService,
    private dataService: DataService,
    private categoryService: CategoryService,
    private roleService: RoleService,
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
    this.searchType = '';

    this.route.queryParams.subscribe(param => {
      if (param.categoryId) {
        if (param.categoryId === '61961b2283bd860013462d86') {
          this.curCategory = 0;
        } else if (param.categoryId === '61961b2a83bd860013462d87') {
          this.curCategory = 1;
        }
      }
      this.router.navigate([], { queryParams: { categoryId: null }, queryParamsHandling: 'merge' });
    });
  }

  public filteredDocuments;  // 순서값으로 후 가공된 전체 포스터목록

  ngOnInit(): void {
    this.loadCategories();

    if (this.user) {
      this.user = JSON.parse(this.user);
      this.roles = this.roleService.getRoles(this.user.roles);
    }
  }

  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  setCategory(categoryIndex): void {
    this.curCategory = categoryIndex;
    this.documents = _.filter(this.allDocuments, document => {
      return this.allCategories[this.curCategory].id === document.categoryId;
    });
  }

  loadCategories = () => {
    this.categoryService.find('document').subscribe(res => {
      this.allCategories = res;

      this.loadDocuments();
    });
  }

  downloadAttachment(document): void {
    window.open(document.fileUrl, '_blank');
  }

  goCreate(): void {
    this.router.navigate(['/document-write'], {
      queryParams: {
        categoryId: this.allCategories[this.curCategory].id,
        categoryName: this.allCategories[this.curCategory].categoryName,
        isModify: false
      }
    });
  }

  goUpdate(documentId): void {
    this.router.navigate(['/document-write'], {
      queryParams: {
        categoryId: this.allCategories[this.curCategory].id,
        categoryName: this.allCategories[this.curCategory].categoryName,
        isModify: true,
        documentId
      }
    });
  }

  confirmDelete(documentId): void {
    if (confirm('Are you sure to delete?')) {
      this.documentService.delete(documentId).subscribe(res => {
        this.loadCategories();
      });

    }
  }

  /**
   * 포스터 목록 조회
   */
  loadDocuments = () => {
    this.documentService.find().subscribe(res => {
      this.allDocuments = res;
      this.documents = _.filter(this.allDocuments, document => {
        return this.allCategories[this.curCategory].id === document.categoryId;
      });
    });
  }

  /**
   * 포스터 목록을 카테고리 별로 정제한다.
   * @param documents 포스터 목록
   */
  sortByCategory = (documents) => {
    return _.chain(documents)
      .groupBy(document => {
        return document.category ? JSON.stringify(document.category) : '{}';
      })
      .map((document, category) => {
        category = JSON.parse(category);
        category.documents = document;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

  /** 카테고리를 선택한다. */
  selectCategory = (category) => {
    if (!category) {
      this.documents = this.allDocuments;
    } else {
      this.documents = _.filter(this.allDocuments, document => {
        return document.category.categoryName === category;
      });
    }
  }

  /** 카테고리 검색 */
  search = () => {
    if (this.searchText) {
      this.documents = _.filter(this.documents, document => {
        return (
          document.title.toLowerCase().includes(this.searchText.toLowerCase()) ||
          document.subTitle.toLowerCase().includes(this.searchText.toLowerCase()) ||
          document.author?.toLowerCase().includes(this.searchText.toLowerCase())
        );
      });
    } else {
      this.documents = _.filter(this.filteredDocuments, document => {
        return this.allCategories[this.curCategory].id === document.categoryId;
      });
    }
  }

  goTop(): void {
    this.functionService.scrollToTop();
  }

  /** 포스터 상세로 페이지 이동 */
  goDetail(documentId: string): void {
    this.router.navigate([`documents/${documentId}`]);
  }
}
