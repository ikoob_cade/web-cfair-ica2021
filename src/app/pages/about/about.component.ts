import * as _ from 'lodash';
import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { EventService } from '../../services/api/event.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AboutComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line: no-input-rename
  @Input('opt') opt = false;

  public event;
  curLang;
  subscriptions = [];


  constructor(
    private eventService: EventService,
    public router: Router,
    private dataService: DataService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  ngOnInit(): void {
    this.loadEventInfo();
  }

  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  loadEventInfo = () => {
    this.eventService.findOne().subscribe(res => {
      res.description = res.descriptionMultilingual;
      res.description2 = res.description2Multilingual;
      this.event = res;
    });
  }

}
