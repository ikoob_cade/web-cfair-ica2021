import * as _ from 'lodash';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { forkJoin as observableForkJoin, Observable, Subscription } from 'rxjs';
import { AgendaService } from '../../../services/api/agenda.service';
import { DateService } from '../../../services/api/date.service';
import { RoomService } from '../../../services/api/room.service';
import { map } from 'rxjs/operators';
import { MemberService } from '../../../services/api/member.service';
import { HistoryService } from '../../../services/api/history.service';
import { SocketService } from '../../../services/socket/socket.service';
import { BannerService } from '../../../services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { DataService } from '../../../services/data.service';
declare var $: any;

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LiveComponent implements OnInit, OnDestroy {
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  public user: any; // 사용자 정보
  public live: any; // 라이브 방송
  public isLiveAgenda: any; // 현재 진행중인 아젠다

  public attendance: boolean; // 출석상태

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.
  public curLang;
  public liveChatUrl;

  public selected: any = {
    date: null,
    room: null
  };

  public relationId;
  public icaYoutubeUrls = {
    '2021-11-28': {
      'Room 1 (Maple)': {
        en: 'https://www.youtube.com/embed/UO_esZ5QpME',
        fr: 'https://www.youtube.com/embed/3RjXexyKKHU',
        es: 'https://www.youtube.com/embed/MicCx-kgqwg',
        ko: 'https://www.youtube.com/embed/YNEFzBXESrA'
      },
      'Room 2 (Lab3)': {
        en: 'https://www.youtube.com/embed/z2g8356p0L8',
        fr: 'https://www.youtube.com/embed/z2g8356p0L8',
        es: 'https://www.youtube.com/embed/z2g8356p0L8',
        ko: 'https://www.youtube.com/embed/6BUaceZvEE4'
      },
      'Room 3 (Lab4)': {
        en: 'https://www.youtube.com/embed/I4-5vfV_pYQ',
        fr: 'https://www.youtube.com/embed/I4-5vfV_pYQ',
        es: 'https://www.youtube.com/embed/I4-5vfV_pYQ',
        ko: 'https://www.youtube.com/embed/bdKk-2ZpMVw'
      },
      'Room 4 (Lab5)': {
        en: 'https://www.youtube.com/embed/8ER_nQuuSNs',
        fr: 'https://www.youtube.com/embed/8ER_nQuuSNs',
        es: 'https://www.youtube.com/embed/L0owmUt9KPo',
        ko: 'https://www.youtube.com/embed/TlT7CAqtcX0'
      },
      'Room 5 (Lab2)': {
        en: 'https://www.youtube.com/embed/pFLq5yWXkdo',
        fr: 'https://www.youtube.com/embed/pFLq5yWXkdo',
        es: 'https://www.youtube.com/embed/RhJfsGmdU44',
        ko: 'https://www.youtube.com/embed/dTbuI_FXKKI'
      },
      'Room 6 (Zoom)': { // ! ZOOM
        en: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/83108757430?pwd=eXEwa2t0N1JyZU5lMlMzelV6T21qUT09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85265047678?pwd=QVRUa2NTSENsenh3c3hka1BybVcxQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83622177460?pwd=dVlqN2hISGlZS0NQUlBjeXZkNU9rdz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86095953300?pwd=dW44QXV2WHdtWllNb0lLVkVhTzFCdz09'
          },
        ],
        fr: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/83108757430?pwd=eXEwa2t0N1JyZU5lMlMzelV6T21qUT09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85265047678?pwd=QVRUa2NTSENsenh3c3hka1BybVcxQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83622177460?pwd=dVlqN2hISGlZS0NQUlBjeXZkNU9rdz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86095953300?pwd=dW44QXV2WHdtWllNb0lLVkVhTzFCdz09'
          },
        ],
        es: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/83108757430?pwd=eXEwa2t0N1JyZU5lMlMzelV6T21qUT09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85265047678?pwd=QVRUa2NTSENsenh3c3hka1BybVcxQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83622177460?pwd=dVlqN2hISGlZS0NQUlBjeXZkNU9rdz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86095953300?pwd=dW44QXV2WHdtWllNb0lLVkVhTzFCdz09'
          },
        ],
        ko: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/83108757430?pwd=eXEwa2t0N1JyZU5lMlMzelV6T21qUT09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85265047678?pwd=QVRUa2NTSENsenh3c3hka1BybVcxQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83622177460?pwd=dVlqN2hISGlZS0NQUlBjeXZkNU9rdz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86095953300?pwd=dW44QXV2WHdtWllNb0lLVkVhTzFCdz09'
          },
        ],
      },
      'Room 7 (Zoom)': { // ! ZOOM
        en: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84047869073?pwd=clE5SnJoanI4OGVDN0Z4SWYrSSt3Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/84845941737?pwd=eXdkMzNxTXd2Rk00NDJhMGdoUGF1QT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/81830509902?pwd=bVVNL1VoQUx2U3Azc3BEY1c5QnFoQT09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86843290927?pwd=cDdRUG9RSmlVTERpdVcwZEhkb2Jwdz09'
          },
        ],
        fr: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84047869073?pwd=clE5SnJoanI4OGVDN0Z4SWYrSSt3Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/84845941737?pwd=eXdkMzNxTXd2Rk00NDJhMGdoUGF1QT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/81830509902?pwd=bVVNL1VoQUx2U3Azc3BEY1c5QnFoQT09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86843290927?pwd=cDdRUG9RSmlVTERpdVcwZEhkb2Jwdz09'
          },
        ],
        es: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84047869073?pwd=clE5SnJoanI4OGVDN0Z4SWYrSSt3Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/84845941737?pwd=eXdkMzNxTXd2Rk00NDJhMGdoUGF1QT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/81830509902?pwd=bVVNL1VoQUx2U3Azc3BEY1c5QnFoQT09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86843290927?pwd=cDdRUG9RSmlVTERpdVcwZEhkb2Jwdz09'
          },
        ],
        ko: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84047869073?pwd=clE5SnJoanI4OGVDN0Z4SWYrSSt3Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/84845941737?pwd=eXdkMzNxTXd2Rk00NDJhMGdoUGF1QT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/81830509902?pwd=bVVNL1VoQUx2U3Azc3BEY1c5QnFoQT09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/86843290927?pwd=cDdRUG9RSmlVTERpdVcwZEhkb2Jwdz09'
          },
        ],
      },
    },
    '2021-11-29': {
      'Room 1 (Maple)': {
        en: 'https://www.youtube.com/embed/1u2CJ68JwQQ',
        fr: 'https://www.youtube.com/embed/nZSOay5RMMw',
        es: 'https://www.youtube.com/embed/FNspijcxd3o',
        ko: 'https://www.youtube.com/embed/xm3pktZBEdE'
      },
      'Room 2 (Lab3)': {
        en: 'https://www.youtube.com/embed/85iD1poQ7x0',
        fr: 'https://www.youtube.com/embed/85iD1poQ7x0',
        es: 'https://www.youtube.com/embed/85iD1poQ7x0',
        ko: 'https://www.youtube.com/embed/VkmIfYUcgFg'
      },
      'Room 3 (Lab4)': {
        en: 'https://www.youtube.com/embed/wMZGI0e3cno',
        fr: 'https://www.youtube.com/embed/wMZGI0e3cno',
        es: 'https://www.youtube.com/embed/wMZGI0e3cno',
        ko: 'https://www.youtube.com/embed/tYCGrwlIXbQ'
      },
      'Room 4 (Lab5)': {
        en: 'https://www.youtube.com/embed/3OIyeFvmjb8',
        fr: 'https://www.youtube.com/embed/3OIyeFvmjb8',
        es: 'https://www.youtube.com/embed/5ef9LRgRZk8',
        ko: 'https://www.youtube.com/embed/PX_ncuCpu3g'
      },
      'Room 5 (Lab2)': {
        en: 'https://www.youtube.com/embed/kh2gt0CjugI',
        fr: 'https://www.youtube.com/embed/kh2gt0CjugI',
        es: 'https://www.youtube.com/embed/8zrXNrt3REk',
        ko: 'https://www.youtube.com/embed/ZT_4TZyQ9js'
      },
      'Room 6 (Zoom)': { // ! ZOOM
        en: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/89352670194?pwd=NE16ejZGcWtwN1RaMnlpNkRjUWQ1Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85187460089?pwd=QjhSdUNNb1VyVU4vMDZkZ05MaXJvdz09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/82773800887?pwd=L2pxdm1EZEtJVHU4dWxHK3VnRktWZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/84870300552?pwd=dVBObEltMTNISFZxSnl0YnczS0VvZz09'
          },
        ],
        fr: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/89352670194?pwd=NE16ejZGcWtwN1RaMnlpNkRjUWQ1Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85187460089?pwd=QjhSdUNNb1VyVU4vMDZkZ05MaXJvdz09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/82773800887?pwd=L2pxdm1EZEtJVHU4dWxHK3VnRktWZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/84870300552?pwd=dVBObEltMTNISFZxSnl0YnczS0VvZz09'
          },
        ],
        es: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/89352670194?pwd=NE16ejZGcWtwN1RaMnlpNkRjUWQ1Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85187460089?pwd=QjhSdUNNb1VyVU4vMDZkZ05MaXJvdz09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/82773800887?pwd=L2pxdm1EZEtJVHU4dWxHK3VnRktWZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/84870300552?pwd=dVBObEltMTNISFZxSnl0YnczS0VvZz09'
          },
        ],
        ko: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/89352670194?pwd=NE16ejZGcWtwN1RaMnlpNkRjUWQ1Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/85187460089?pwd=QjhSdUNNb1VyVU4vMDZkZ05MaXJvdz09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/82773800887?pwd=L2pxdm1EZEtJVHU4dWxHK3VnRktWZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/84870300552?pwd=dVBObEltMTNISFZxSnl0YnczS0VvZz09'
          },
        ],
      },
      'Room 7 (Zoom)': { // ! ZOOM
        en: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84308565790?pwd=VktpZ0dkMTRqc3VjV25PbnozWDl2Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/81256197455?pwd=Y1FSczdHRVUwdUdSNUdnZVNqaGtzQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83755126092?pwd=SFlubjlsQ2szeDVTc09UUUJRNlpBZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/81603879780?pwd=amhPK2NkV3Z0UFpyTXRIWFBjdWpuQT09'
          },
        ],
        fr: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84308565790?pwd=VktpZ0dkMTRqc3VjV25PbnozWDl2Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/81256197455?pwd=Y1FSczdHRVUwdUdSNUdnZVNqaGtzQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83755126092?pwd=SFlubjlsQ2szeDVTc09UUUJRNlpBZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/81603879780?pwd=amhPK2NkV3Z0UFpyTXRIWFBjdWpuQT09'
          },
        ],
        es: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84308565790?pwd=VktpZ0dkMTRqc3VjV25PbnozWDl2Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/81256197455?pwd=Y1FSczdHRVUwdUdSNUdnZVNqaGtzQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83755126092?pwd=SFlubjlsQ2szeDVTc09UUUJRNlpBZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/81603879780?pwd=amhPK2NkV3Z0UFpyTXRIWFBjdWpuQT09'
          },
        ],
        ko: [
          {
            textValue: '21:00-22:30(KST)',
            url: 'https://us02web.zoom.us/j/84308565790?pwd=VktpZ0dkMTRqc3VjV25PbnozWDl2Zz09'
          },
          {
            textValue: '23:00-00:30(KST)',
            url: 'https://us02web.zoom.us/j/81256197455?pwd=Y1FSczdHRVUwdUdSNUdnZVNqaGtzQT09'
          },
          {
            textValue: '00:30-02:00(KST)',
            url: 'https://us02web.zoom.us/j/83755126092?pwd=SFlubjlsQ2szeDVTc09UUUJRNlpBZz09'
          },
          {
            textValue: '02:30-04:00(KST)',
            url: 'https://us02web.zoom.us/j/81603879780?pwd=amhPK2NkV3Z0UFpyTXRIWFBjdWpuQT09'
          },
        ],
      },
    },
    '2021-11-30': {
      'Room 1 (Vista)': {
        en: 'https://www.youtube.com/embed/sujz2oUNqAM',
        fr: 'https://www.youtube.com/embed/8nqWHczC2y0',
        es: 'https://www.youtube.com/embed/FqLE53KSKoo',
        ko: 'https://www.youtube.com/embed/4fSXVsni6Xw'
      },
      'Room 2 (Walker Hall 1)': {
        en: '',
        fr: '',
        es: '',
        ko: ''
      },
      'Room 3 (Walker Hall 2)': {
        en: '',
        fr: '',
        es: '',
        ko: ''
      },
      'Room 4 (Art Hall)': {
        en: '',
        fr: '',
        es: '',
        ko: ''
      },
      'Room 5 (Grand Hall 4)': {
        en: '',
        fr: '',
        es: '',
        ko: ''
      },
      'Room 6 (Zoom)': { // ! ZOOM
        en: '',
        fr: '',
        es: '',
        ko: '',
      },
      'Room 7 (Zoom)': { // ! ZOOM
        en: '',
        fr: '',
        es: '',
        ko: '',
      },
    },
    '2021-12-01': {
      'Room 1 (Vista)': {
        en: 'https://www.youtube.com/embed/QAKHcsWOptw',
        fr: 'https://www.youtube.com/embed/u0jdbFh_dis',
        es: 'https://www.youtube.com/embed/NmRKiyXEyK8',
        ko: 'https://www.youtube.com/embed/1LHrmnvXr48'
      },
      'Room 2 (Walker Hall 1)': {
        en: 'https://www.youtube.com/embed/sz8JwctZX8A',
        fr: 'https://www.youtube.com/embed/cjQZKrHTrmo',
        es: 'https://www.youtube.com/embed/mNFQGmbk_OM',
        ko: 'https://www.youtube.com/embed/X7WtlQlP72A'
      },
      'Room 3 (Walker Hall 2)': {
        en: 'https://www.youtube.com/embed/AunP25U-rU0',
        fr: 'https://www.youtube.com/embed/c7jepDrIE3I',
        es: 'https://www.youtube.com/embed/czR_claz7vI',
        ko: 'https://www.youtube.com/embed/y8s8U3dpNmI'
      },
      'Room 4 (Art Hall)': {
        en: 'https://www.youtube.com/embed/6Fj2WEPK30Q',
        fr: 'https://www.youtube.com/embed/1kEZgity-xw',
        es: 'https://www.youtube.com/embed/50LUFj_Jnhs',
        ko: 'https://www.youtube.com/embed/wzZaAvHWpgg'
      },
      'Room 5 (Grand Hall 4)': {
        en: 'https://www.youtube.com/embed/ayIDinssmZQ',
        fr: 'https://www.youtube.com/embed/KRZ6R919Fwg',
        es: 'https://www.youtube.com/embed/d9A4sgjJnr4',
        ko: 'https://www.youtube.com/embed/GMSwVvK3iYY'
      },
    },
    '2021-12-02': {
      'Room 1 (Vista)': {
        en: 'https://www.youtube.com/embed/BQ_bIKYzHa4',
        fr: 'https://www.youtube.com/embed/_zEzh3_kXGw',
        es: 'https://www.youtube.com/embed/QxlM5XNr7IM',
        ko: 'https://www.youtube.com/embed/6VFwaJVS_0c'
      },
      'Room 2 (Walker Hall 1)': {
        en: 'https://www.youtube.com/embed/pjbjUSwRt-k',
        fr: 'https://www.youtube.com/embed/oGKOJfeo6tY',
        es: 'https://www.youtube.com/embed/gAexwOOGhCk',
        ko: 'https://www.youtube.com/embed/5eRJsrX8-5I'
      },
      'Room 3 (Walker Hall 2)': {
        en: 'https://www.youtube.com/embed/If-DPX8RoWw',
        fr: 'https://www.youtube.com/embed/PDEJOajZi7s',
        es: 'https://www.youtube.com/embed/hHJL2gXrKDk',
        ko: 'https://www.youtube.com/embed/ZCzjS70N7AA'
      },
      'Room 4 (Art Hall)': {
        en: 'https://www.youtube.com/embed/99mwAMVuvkM',
        fr: 'https://www.youtube.com/embed/mTP2CpzOyR8',
        es: 'https://www.youtube.com/embed/53dmriwk4gc',
        ko: 'https://www.youtube.com/embed/nHlb0frmAAM'
      },
      'Room 5 (Grand Hall 4)': {
        //   en: 'https://player.vimeo.com/video/652326479',
        //   fr: 'https://player.vimeo.com/video/652352313',
        //   es: 'https://player.vimeo.com/video/652327661',
        //   ko: 'https://player.vimeo.com/video/652327300'
        // },
        en: 'https://www.youtube.com/embed/p4oPJJ20wdA',
        fr: 'https://www.youtube.com/embed/NrabnRKqMY8',
        es: 'https://www.youtube.com/embed/GhROUSZU_As',
        ko: 'https://www.youtube.com/embed/LnkTQeySW_I'
      },
    },
    '2021-12-03': {
      'Room 1 (Vista)': {
        en: 'https://www.youtube.com/embed/gNRitJWP144',
        fr: 'https://www.youtube.com/embed/WQLv-4NwZxM',
        es: 'https://www.youtube.com/embed/wppXep7Eu_k',
        ko: 'https://www.youtube.com/embed/W2TArfwROXo'
      },
      'Room 2 (Walker Hall 1)': {
        en: 'https://www.youtube.com/embed/S-jDmrsd-o8',
        fr: 'https://www.youtube.com/embed/8HOlqxyEO8o',
        es: 'https://www.youtube.com/embed/7M8KHnbh9gE',
        ko: 'https://www.youtube.com/embed/oHdoM4Qu9B4'
      },
      'Room 3 (Walker Hall 2)': {
        en: 'https://www.youtube.com/embed/ECM3dXvISdE',
        fr: 'https://www.youtube.com/embed/_A3AVFhFrwI',
        es: 'https://www.youtube.com/embed/cYbq79cXEfY',
        ko: 'https://www.youtube.com/embed/plXqIKA1Fqs'
      },
      'Room 4 (Art Hall)': {
        en: 'https://www.youtube.com/embed/avf522DYu6w',
        fr: 'https://www.youtube.com/embed/aPfRLXQF8YY',
        es: 'https://www.youtube.com/embed/5u8n_TfXRoM',
        ko: 'https://www.youtube.com/embed/bXUpsFHSe7Y'
      },
      'Room 5 (Grand Hall 4)': {
        en: 'https://www.youtube.com/embed/esSEM_Gl2rY',
        fr: 'https://www.youtube.com/embed/_6DghuoGXb4',
        es: 'https://www.youtube.com/embed/4A4MMZTlA-4',
        ko: 'https://www.youtube.com/embed/KGq7VmQ8sAM'
      },
    },
  };

  public showPlayer = false;

  public banners = []; // 광고배너 리스트
  public bannersLiveChat = []; // 플레이어에 넘겨줄 채팅창 위 롤링배너 데이타
  public classColMd = false;

  public next: any;

  // 언어변경 감지를 위한 구독데이터 목록
  private subscriptions = [];

  private isLoaded = false;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private dateService: DateService,
    private roomService: RoomService,
    private agendaService: AgendaService,
    private memberService: MemberService,
    private historyService: HistoryService,
    private socketService: SocketService,
    private bannerService: BannerService,
    private dataService: DataService,
  ) {
    this.doInit();

    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;

        if (this.isLoaded) {
          this.onChangeLanguage();
        }
      });

    this.subscriptions.push(languageSubscription);
  }

  onChangeLanguage(): void {
    this.live = null;
    setTimeout(() => {
      this.live = this.icaYoutubeUrls[this.selected.date.date][this.selected.room.roomName][this.curLang];
    }, 100);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (window.innerWidth < 1070) {
      this.classColMd = true;
    } else {
      this.classColMd = false;
    }
  }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    this.getBanners();

    if (window.innerWidth < 1070) {
      this.classColMd = true;
    } else {
      this.classColMd = false;
    }
  }

  doInit() {
    this.historyService.setAttendance('in');
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          this.dates = resp[0];
          this.rooms = resp[1];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[5];
        let room = this.rooms[0];
        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId && param.roomId) {
            date = this.dates.find(date => {
              return date.id === param.dateId;
            });
            room = this.rooms.find(room => {
              return room.id === param.roomId;
            });
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });
        this.selected = { date, room };
        this.setAgendaList(date, room);
      });
  }

  isZoom = false;

  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean): any {
    this.historyService.setRoom(room);

    /*
    * 날짜의 옵션값 확인해서 player컴포넌트 출력을 제어한다.
    * 선택한 날짜의 ON/OFF 상태를 확인해서 플레이어, 채팅 이용 불가하도록 하기 위함
    */
    /** ICA 줌미팅룸 전용 날짜
     * Pre Congress기간 내 Room6,7
     * 2021-11-28: ObjectId("6156c44f1a21ea0013439637")
     * 2021-11-29: ObjectId("611eefecbe4ba9001371c889")
     * 2021-11-30: ObjectId("612db1f7fa3182001279df01")
     * Room 6: ObjectId("619b5543b19d9000134edbcc")
     * Room 7: ObjectId("619b5550b19d9000134edbcd")
     *
     * 위 내용으로 접근하면 플레이어 강제 OFF, 각각 설정된(현재언어조건까지 포함) 줌URL 안내화면 출력
     */
    if (
      (
        date.id === '6156c44f1a21ea0013439637'
        || date.id === '611eefecbe4ba9001371c889'
        || date.id === '612db1f7fa3182001279df01'
      )
      &&
      (
        room.id === '619b5543b19d9000134edbcc'
        || room.id === '619b5550b19d9000134edbcd'
      )
    ) {
      this.showPlayer = false;
      this.isZoom = true;
    } else {
      this.isZoom = false;
      if (!date.showPlayer) {
        this.showPlayer = false;
      } else {
        this.showPlayer = true;
      }
    }

    setTimeout(() => {
      this.socketService.leave(this.selected.room.id);
      this.socketService.join(room.id);
    }, this.user ? 0 : 300);

    if (this.live && !this.historyService.isAttended()) {
      this.openEntryAlert();
      if (!this.showPlayer) {
        this.showPlayer = !this.showPlayer;
      }

      this.next = () => {
        this.setAgendaList(date, room, true);
      };
    } else {
      if (next) {
        this.next = null;
      }
      setTimeout(() => {
        this.selected = {
          date,
          room
        };
        this.getAgendasV2(date.id, room.id)
          .subscribe(agendas => {
            this.agendas = agendas;
            setTimeout(() => {
              this.live = this.icaYoutubeUrls?.[this.selected.date.date]?.[this.selected.room.roomName]?.[this.curLang];
              this.relationId = this.selected.room.id;
              this.isLoaded = true;
              // 별도의 채팅창 포함 여부 확인
              // if (this.live && this.live.chatIncluded && this.live.chatUrl) {
              //   this.liveChatUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.live.chatUrl);
              // }

              // Live가 있으면 IN / OUT
              if (this.live) {
                if (this.showPlayer) {
                  this.openEntryAlert();
                }
                this.checkLiveAgenda();
              }
            }, 100);
          });
        this.live = null; // new app-player
      }, 500);
    }
  }

  /**
   * 입장/퇴장 모달을 출력한다.
   * user의 isLog데이터 구분.
   * 해외연자는 연수평점의 의무가 없어서 입장/퇴장이 불필요하다.
   */
  openEntryAlert(): void {
    // 로그 저장하지 않는 경우
    if (!this.user.isLog) {
      return;
    } else {
      // 퇴장 팝업은 생략하고 서버로 전송한다.
      if (this.getAttendance() === 'out') {
        this.atndnCheck();
      } else {
        this.entryAlertBtn.nativeElement.click();
      }
    }
  }

  // 현재 진행중인 아젠다 확인
  checkLiveAgenda(): void {
    this.isLiveAgenda = this.agendas[0];
    this.agendas.forEach((agenda: any) => {
      const agendaDate = agenda.date.date.replace(/-/gi, '/');
      const agendaStartTime = new Date(`${agendaDate}/${agenda.startTime}`);
      const agendaEndTime = new Date(`${agendaDate}/${agenda.endTime}`);
      const now: Date = new Date();
      if (now > agendaStartTime && now < agendaEndTime) {
        this.isLiveAgenda = agenda;
      }
    });
  }

  /* 아젠다 목록 조회 V2
   * date, room 값을 파라미터에 추가했음.
   * 위 조건에 해당하는 아젠다만 받아온다 => 서버 부하 감소
   */
  getAgendasV2(dateId, roomId): Observable<any> {
    return this.agendaService.findV2(dateId, roomId);
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * 강의 상세보기
   * @param agenda 강의
   */
  goDetail(agenda: any): void {
    this.router.navigate([`/live/${this.selected.date.id}/${this.selected.room.id}/${agenda.id}`]);
  }

  // 출석체크
  atndnCheck(isMove?): void {
    if (!this.user.isLog) {
      // 로그 저장 안함
      return;
    }

    const options: { relationId: string, relationType: string, logType: string } = {
      relationId: this.selected.room.id,
      relationType: 'room',
      logType: this.historyService.getAttendance()
    };

    sessionStorage.setItem('currentRoom', this.selected.room.id);
    this.memberService.history(this.user.id, options)
      .subscribe(() => {
        if (this.historyService.isAttended()) {
          this.historyService.setAttendance('out');
        } else {
          this.historyService.setAttendance('in');
        }

        if (this.next) {
          this.next();
        }

        if (isMove) {
          this.openEntryAlert();
        }
      },
        (error) => {
          console.error(error);
        });
  }

  // 참여 상태 출력
  getAttendance(): string {
    return this.historyService.isAttended() ? 'in' : 'out';
  }


  // ! -------------------  Banner Slider  -------------------
  // 슬라이드 배너 신경외과용이니 나중에 복사한다.
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (res.live) {
        res.live.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
      // const bannersLiveChat = [];
      // res.liveChat.forEach(item => {
      //   const data = {
      //     link: item.link,
      //     thumbImage: item.photoUrl,
      //     alt: item.title,
      //   };
      //   bannersLiveChat.push(data);
      // });

      // if (bannersLiveChat.length > 0) {
      //   this.bannersLiveChat = bannersLiveChat;
      // }
    });
  }

  // 광고 구좌 왼쪽 버튼 클릭
  slidePrev(target): void {
    this[target].prev();
  }
  // 광고 구좌 오른쪽 버튼 클릭
  slideNext(target): void {
    this[target].next();
  }
  // 광고 배너 클릭
  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

  /**
   * 페이지 나갈 때 처리
   */
  ngOnDestroy(): void {
    this.socketService.leave(this.selected.room.id);
    this.historyService.setRoom(null);

    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }


  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) history('out')을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event): any {
    // 로그 저장하는 경우
    if (!this.historyService.isAttended() && this.user.isLog) {
      this.atndnCheck(true);
    }

    $event.preventDefault();
    return false;
  }
}
