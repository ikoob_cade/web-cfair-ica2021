import * as _ from 'lodash';
import { UAParser } from 'ua-parser-js';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { NgImageSliderComponent } from 'ng-image-slider';
import { BannerService } from '../../../services/api/banner.service';
import { MemberService } from '../../../services/api/member.service';
import { HistoryService } from '../../../services/api/history.service';
import { AgendaService } from '../../../services/api/agenda.service';
import { VodService } from '../../../services/api/vod.service';
import { RoomService } from '../../../services/api/room.service';
import { DataService } from '../../../services/data.service';
import { Subscription } from 'rxjs';
import { RoleService } from '../../../services/roleService.service';

declare let $: any;
@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit, AfterViewInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;

  public banners = [];

  /** 정답 확인 후 여부 */
  public beforeSubmit = true;

  public next: any;
  private user: any = sessionStorage.getItem('cfair');

  public isSingleCorrect = true;
  public originAgenda;

  public agenda: any;
  public agendaId: string;

  public content: any;

  public roles: any = {};

  selectedAgedna: any = [];
  public newInput;


  public selected: any = {
    date: null,
    room: null
  };

  parentAgenda;
  rooms;

  curLang;
  isLoaded = false;

  subscriptions = [];

  private preRoomNames =
    ['Room 1 (Maple)', 'Room 2 (Lab3)', 'Room 3 (Lab4)',
      'Room 4 (Lab5)', 'Room 5 (Lab2)', 'Room 6 (Zoom)',
      'Room 7 (Zoom)'];

  private mainRoomNames =
    ['Room 1 (Vista)', 'Room 2 (Walker Hall 1)', 'Room 3 (Walker Hall 2)',
      'Room 4 (Art Hall)', 'Room 5 (Grand Hall 4)'];

  constructor(
    private activatedRoute: ActivatedRoute,
    private agendaService: AgendaService,
    private bannerService: BannerService,
    private memberService: MemberService,
    private historyService: HistoryService,
    private vodService: VodService,
    private roomService: RoomService,
    private router: Router,
    private dataService: DataService,
    private roleService: RoleService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;

        if (this.isLoaded) {
          this.onChangeLanguage();
        }
      });
    this.subscriptions.push(languageSubscription);

    if (this.user) {
      this.user = JSON.parse(this.user);
      this.roles = this.roleService.getRoles(this.user.roles);
    }
  }

  onChangeLanguage(): void {
    const curLang = this.curLang;
    this.curLang = null;
    setTimeout(() => {
      this.curLang = curLang;
    }, 100);
  }

  ngOnInit(): void {
    this.historyService.setAttendance('in');

    this.activatedRoute.params.subscribe(params => {
      if (params.agendaId) {
        this.agendaService.findOne(params.agendaId).subscribe(agenda => {
          this.agenda = agenda;
          this.content = agenda.contents;
          this.agendaId = agenda.id;
          this.getVodComments();
          // this.getParentAgenda();
          this.getRooms();
          this.isLoaded = true;
        });
      }
    });
  }

  getParentAgenda(): void {
    if (this.agenda.parentId) {
      this.agendaService.findOne(this.agenda.parentId).subscribe(agenda => {
        this.parentAgenda = agenda;
      });
    }
  }

  getRooms(): void {
    this.roomService.find().subscribe(res => {
      this.rooms = res;
      const selectedRoom = _.find(this.rooms, room => room.id === this.agenda.roomId);

      this.setRoomNames(this.agenda.date.date);
      this.selected = { room: selectedRoom, vodAgenda: this.agenda };
    });
  }

  setRoomNames(date): void {
    const preDates = ['2021-11-28', '2021-11-29', '2021-11-30'];
    const mainDates = ['2021-12-01', '2021-12-02', '2021-12-03'];

    if (preDates.includes(date)) {
      _.forEach(this.rooms, (room, index) => room.roomName = this.preRoomNames[index]);
    } else if (mainDates.includes(date)) {
      _.forEach(this.rooms, (room, index) => room.roomName = this.mainRoomNames[index]);
    }
  }

  setAgendaList($event, room): void {
    this.router.navigate(['/vod'], { queryParams: { dateId: this.agenda.dateId, roomId: room.id } });
  }

  ngAfterViewInit(): void {
  }

  // ! Banner Slider
  slidePrev(target): void {
    this[target].prev();
  }

  slideNext(target): void {
    this[target].next();
  }

  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

  // 리스트로 돌아가기
  goBack(): void {
    this.router.navigate(['/vod'], { queryParams: { dateId: this.agenda.dateId, roomId: this.agenda.roomId } });
  }

  // ! HTTP
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (_.keys(res).length > 0) {
        res.vod.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
    });
  }

  // ! 댓글
  getVodComments(): void {
    this.agenda.comments = { commentList: [], count: 0 };

    this.vodService.findComments(this.agendaId).subscribe(res => {
      this.agenda.comments = res;
    });
  }

  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  /** 댓글 생성 */
  createComment(parentId?: string): void {
    const parser = new UAParser();
    const fullUserAgent = parser.getResult();

    if (parentId) {
      if (!$(`#cInput_${parentId}`)[0].value) {
        return alert('질문을 입력하세요.');
      }
    } else if (!this.newInput) {
      return alert('질문을 입력하세요.');
    }

    const body: any = {
      description: parentId ? $(`#cInput_${parentId}`)[0].value : this.newInput,
      memberId: this.user.id,
      parentId: parentId ? parentId : '',
      level: parentId ? 1 : 0,
      userAgent: JSON.stringify(fullUserAgent),
      browser: JSON.stringify(fullUserAgent.browser),
      device: JSON.stringify(fullUserAgent.device),
      engine: JSON.stringify(fullUserAgent.engine),
      os: JSON.stringify(fullUserAgent.os),
      ua: JSON.stringify(fullUserAgent.ua),
    };

    this.vodService.createComment(this.agendaId, body).subscribe(res => {
      this.newInput = '';
      this.getVodComments();
    });
  }

  /** 답글 Input 활성화 */
  openReply(selectedCommentId): void {
    const cInputWrapper = $(`#cInput_wrapper_${selectedCommentId}`)[0];
    if (cInputWrapper.style.display === 'none' || !cInputWrapper.style.display) {
      cInputWrapper.style.display = 'block';
      $(`#cInput_${selectedCommentId}`)[0].focus();
    } else {
      cInputWrapper.style.display = 'none';
    }
  }

  /** 댓글 수정 활성화 */
  modify(inputId): void {
    const input = $(`#${inputId}`)[0];
    input.disabled = false;
    input.focus();
  }

  update(commentId, type): void {
    let input;
    if (type === 'child') {
      input = $(`#cInput_${commentId}`)[0];
    } else {
      input = $(`#pInput_${commentId}`)[0];
    }

    if (!input.value) {
      return alert('댓글을 입력하세요.');
    }

    const body = {
      description: input.value,
    };

    this.vodService.updateComment(this.agendaId, commentId, body).subscribe(res => {
      this.getVodComments();
    });
  }

  isModify(inputId): boolean {
    if ($(`#${inputId}`) && $(`#${inputId}`)[0]) {
      return !$(`#${inputId}`)[0].disabled;
    }
    return false;
  }

  cancel(): void {
    this.getVodComments();
  }

  /** 댓글을 삭제한다 */
  remove(commentId): void {
    if (confirm('댓글을 삭제하시겠습니까?')) {
      this.vodService.deleteComment(this.agendaId, commentId).subscribe(res => {
        this.getVodComments();
      });
    }
  }

}
