import * as _ from 'lodash';
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../services/data.service';
import { forkJoin as observableForkJoin, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DateService } from '../../../services/api/date.service';
import { AgendaService } from '../../../services/api/agenda.service';
import { RoomService } from '../../../services/api/room.service';

@Component({
  selector: 'app-vod',
  templateUrl: './vod.component.html',
  styleUrls: ['./vod.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VodComponent implements OnInit, AfterViewInit, OnDestroy {
  curLang;
  subscriptions = [];
  user: any = JSON.parse(sessionStorage.getItem('cfair'));
  public selected: any = {
    date: null,
    room: null
  };

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.

  private preRoomNames =
    ['Room 1 (Maple)', 'Room 2 (Lab3)', 'Room 3 (Lab4)',
      'Room 4 (Lab5)', 'Room 5 (Lab2)', 'Room 6 (Zoom)',
      'Room 7 (Zoom)'];

  private mainRoomNames =
    ['Room 1 (Vista)', 'Room 2 (Walker Hall 1)', 'Room 3 (Walker Hall 2)',
      'Room 4 (Art Hall)', 'Room 5 (Grand Hall 4)'];

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private dataService: DataService,
    private dateService: DateService,
    private agendaService: AgendaService,
    private roomService: RoomService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  ngOnInit(): void {
    this.doInit();
  }
  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }
  ngAfterViewInit(): void {
  }

  doInit(): void {
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          this.dates = resp[0];
          this.rooms = resp[1];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[0];
        let room = this.rooms[0];

        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId) {
            date = this.dates.find(date => {
              return date.id === param.dateId;
            });
          }
          if (param.roomId) {
            room = this.rooms.find(room => {
              return room.id === param.roomId;
            });
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });


        this.selected = { date, room };
        this.setRoomNames(date);

        this.getAgendasV2(date.id, room.id).subscribe(res => {
          this.agendas = res.filter(agenda => agenda.isDetail);
        });
      });
  }

  setRoomNames(date): void {
    const preDates = ['2021-11-28', '2021-11-29', '2021-11-30'];
    const mainDates = ['2021-12-01', '2021-12-02', '2021-12-03'];

    if (preDates.includes(date.date)) {
      _.forEach(this.rooms, (room, index) => room.roomName = this.preRoomNames[index]);
    } else if (mainDates.includes(date.date)) {
      _.forEach(this.rooms, (room, index) => room.roomName = this.mainRoomNames[index]);
    }
  }

  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean): any {
    setTimeout(() => {
      this.selected = {
        date,
        room
      };
      this.setRoomNames(date);

      this.getAgendasV2(date.id, room.id)
        .subscribe(res => {
          this.agendas = res.filter(agenda => agenda.isDetail);
        });
    }, 500);
  }

  // 상세보기
  goAgendaDetail(agenda): void {
    if (agenda.contents) {
      this.router.navigate([`/vod/${agenda.id}`]);
    }
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  getAgendasV2(dateId, roomId): Observable<any> {
    return this.agendaService.findV2(dateId, roomId);
  }

  goDetail(vod): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

}
