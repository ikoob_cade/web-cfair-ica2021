import { Component, OnInit } from '@angular/core';
import { VodService } from '../../services/api/vod.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {
  public vods: any[] = [];
  public categories: any;
  public programBookUrl = '';
  public excerptUrl = '';
  public user;
  constructor(
    private router: Router,
    private vodService: VodService
  ) {
    this.getVod();
  }

  ngOnInit(): void {
    this.user = sessionStorage.getItem('cfair');
  }

  public getVod(): void {
    this.vodService.find().subscribe((response: any) => {
      this.categories = this.sortByCategory(response);
      // this.vods = response;
    });
  }

  public goDetail(vod: any): void {
    this.router.navigate([`/vod/${vod.id}`]);
  }

  sortByCategory = (posters) => {
    return _.chain(posters)
      .groupBy(poster => {
        return poster.category ? JSON.stringify(poster.category) : '{}';
      })
      .map((poster, category) => {
        category = JSON.parse(category);
        category.vods = poster;
        return category;
      }).sortBy(category => {
        return category.seq;
      })
      .value();
  }

}
