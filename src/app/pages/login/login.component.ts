import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UAParser } from 'ua-parser-js';
import { FormBuilder, Validators } from '@angular/forms';
declare var $: any;

enum AUTH_ERROR_MESSAGE {
  UNAUTHORIZED = 'Email과 Password가 일치하지 않습니다.',
  NOT_FOUND = 'Email과 Password가 일치하지 않습니다.',
  ENTER_ALL = 'Please complete all fields',
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  private user: any;
  public errorMessage = ''; // 로그인 에러메세지
  public formGroup: any;

  constructor(
    public router: Router,
    public auth: AuthService,
    private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      email: [''],
      password: ['']
    });

    this.formGroup.controls.email.valueChanges.subscribe(control => {
      if (control) {
        this.formControls.email.setValidators([
          Validators.required,
          Validators.pattern('[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]
        );
        this.formControls.email.updateValueAndValidity({ emitEvent: false });
      } else {
        this.formControls.email.clearValidators();
        this.formControls.email.updateValueAndValidity({ emitEvent: false });
      }
    });

    this.formGroup.controls.password.valueChanges.subscribe(control => {
      if (control) {
        this.formControls.password.setValidators(Validators.required);
        this.formControls.password.updateValueAndValidity({ emitEvent: false });
      } else {
        this.formControls.password.clearValidators();
        this.formControls.password.updateValueAndValidity({ emitEvent: false });
      }
    });
  }

  public get formControls(): any {
    return this.formGroup.controls;
  };

  ngOnInit(): void {
  }

  // 로그인 Submit
  login(): void {
    if (this.loginValidator()) {
      const parser = new UAParser();
      const fullUserAgent = parser.getResult();

      const body: any = {
        ...this.formGroup.value,
        userAgent: JSON.stringify(fullUserAgent),
        browser: JSON.stringify(fullUserAgent.browser),
        device: JSON.stringify(fullUserAgent.device),
        engine: JSON.stringify(fullUserAgent.engine),
        os: JSON.stringify(fullUserAgent.os),
        ua: JSON.stringify(fullUserAgent.ua),
      };

      this.auth.login(body).subscribe(async res => {
        if (res.token) {
          this.user = res;
          this.errorMessage = '';

          this.goToMain();
        }
      }, error => {
        if (error.status === 401) {
          this.errorMessage = AUTH_ERROR_MESSAGE.UNAUTHORIZED;
        }
        if (error.status === 404) {
          this.errorMessage = AUTH_ERROR_MESSAGE.NOT_FOUND;
        }
      });
    } else {
      alert(AUTH_ERROR_MESSAGE.ENTER_ALL);
    }
  }

  // 로그인 유효성 검사
  loginValidator(): boolean {
    if (this.formGroup.invalid) {
      return false;
    }
    if (!this.formControls.email.value || !this.formControls.password.value) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    sessionStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }
}
