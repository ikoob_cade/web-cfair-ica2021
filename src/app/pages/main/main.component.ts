import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';
import { EventService } from '../../services/api/event.service';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';
import { DayjsService } from '../../services/dayjs.service';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;
  public speakers: Array<any> = [];
  public mobile: boolean;

  public user;

  private event;

  public speakerDetail;
  public speakerLoaded = false;
  curLang;

  private koreaTimer;
  public koreaTime = this.dayjsService.getNow().format('YYYY - MM - DD HH:mm:ss');

  private subscriptions = [];

  constructor(
    private speakerService: SpeakerService,
    private eventService: EventService,
    private router: Router,
    private dataService: DataService,
    private dayjsService: DayjsService,
    private translateService: TranslateService,
    private cookieService: CookieService,
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  goTimetable(focusTo): void {
    this.router.navigate(['program'], { queryParams: { focusTo } });
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = this.dayjsService.getNow().format(' - YYYY - MM - DD / HH : mm : ss');
    }, 1000);
  }
  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)];
      })
    );
  }

  ngOnInit(): void {
    this.user = sessionStorage.getItem('cfair');
    this.loadSpeakers();
    this.checkEventVersion();
    this.getTime();
  }

  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });

    clearInterval(this.koreaTimer);
  }

  // 버전 확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(this.event.eventCode + 'ver');
        if (!clientVersion) {
          localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
            location.reload();
          }
        }
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.cookieService.get(`${environment.eventId}_main_popup`)) {
      const cookieTime = this.cookieService.get(`${environment.eventId}_main_popup`);
      const now = new Date().getTime().toString();
      if (cookieTime < now) {
        this.cookieService.delete(`${environment.eventId}_main_popup`);
        if (this.user) {
          this.entryAlert.nativeElement.click();
        }
      }
    } else {
      if (this.user) {
        // ! 메인 팝업
        setTimeout(() => {
          this.entryAlert.nativeElement.click();
        }, 0);
      }
    }
    this.checkEventVersion();
  }

  // 발표자 목록 조회
  loadSpeakers(): void {
    const limit = 4;
    this.speakerService.find(false, true).subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  noModal(): void {
    this.cookieService.set(`${environment.eventId}_main_popup`, 'true', { expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 12) });
  }

  getDetail(speaker): void {
    setTimeout(() => {
      this.speakerLoaded = false;
    }, 0);

    if (this.user) {
      this.speakerService.findOne(speaker.id).subscribe(res => {
        this.speakerDetail = res;
        this.speakerLoaded = true;
        $('#speakersModal').modal('show');
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

}
