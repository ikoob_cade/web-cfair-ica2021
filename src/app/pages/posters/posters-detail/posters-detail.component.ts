import * as _ from 'lodash';
import { UAParser } from 'ua-parser-js';
import { Component, HostListener, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PosterService } from '../../../services/api/poster.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from '../../../services/data.service';
import { Subscription } from 'rxjs';
declare let $: any;
@Component({
  selector: 'app-posters-detail',
  templateUrl: './posters-detail.component.html',
  styleUrls: ['./posters-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PostersDetailComponent implements OnInit, OnDestroy {
  @ViewChild('pdfViewer') pdfViewer: any;

  posterId: string;
  selectedPoster: any = [];

  slide = null;
  slideCurrentPage = 1;
  slideTotalPage;

  videoUrl;

  curLang;

  public user;
  public newInput;

  private subscriptions = [];

  constructor(
    public route: ActivatedRoute,
    private posterService: PosterService,
    private sanitizer: DomSanitizer,
    private dataService: DataService
  ) {
    this.posterId = route.snapshot.params.posterId;
    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  ngOnInit(): void {
    this.getPosterDetail();
  }

  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  getPosterDetail(): void {
    this.posterService.findOne(this.posterId).subscribe(res => {
      this.selectedPoster = res;
      if (this.selectedPoster.contents && this.selectedPoster.contents.contentUrl.includes('.pdf')) {
        this.slide = {
          url: this.selectedPoster.contents.contentUrl,
          credential: true,
        };
      } else if (this.selectedPoster.contents && this.selectedPoster.contents.contentType === 'video') {
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.selectedPoster.contents.contentUrl);
      }
    });
  }

  loadedSlide(event): void {
    event.getPage(1);
    this.slideTotalPage = event._pdfInfo.numPages;
  }

  setSlide(amount: number): void {
    if (this.slideCurrentPage <= this.slideTotalPage) {
      this.slideCurrentPage += amount;
    }
  }

  getPosterComments(): void {
    this.selectedPoster.comments = { commentList: [], count: 0 };

    this.posterService.findComments(this.posterId).subscribe(res => {
      this.selectedPoster.comments = res;
    });
  }

  createComment(parentId?: string): void {
    const parser = new UAParser();
    const fullUserAgent = parser.getResult();

    const body: any = {
      description: parentId ? $(`#cInput_${parentId}`)[0].value : this.newInput,
      memberId: this.user.id,
      parentId: parentId ? parentId : '',
      level: parentId ? 1 : 0,
      userAgent: JSON.stringify(fullUserAgent),
      browser: JSON.stringify(fullUserAgent.browser),
      device: JSON.stringify(fullUserAgent.device),
      engine: JSON.stringify(fullUserAgent.engine),
      os: JSON.stringify(fullUserAgent.os),
      ua: JSON.stringify(fullUserAgent.ua),
    };
    this.posterService.createComment(this.posterId, body).subscribe(res => {
      this.newInput = '';
      this.getPosterComments();
    });
  }

  /** 답글 Input 활성화 */
  openReply(selectedCommentId): void {
    const cInputWrapper = $(`#cInput_wrapper_${selectedCommentId}`)[0];
    if (cInputWrapper.style.display === 'none' || !cInputWrapper.style.display) {
      cInputWrapper.style.display = 'block';
      $(`#cInput_${selectedCommentId}`)[0].focus();
    } else {
      cInputWrapper.style.display = 'none';
    }
  }

  /** 댓글 수정 활성화 */
  modify(inputId): void {
    const input = $(`#${inputId}`)[0];
    input.disabled = false;
    input.focus();
  }

  update(commentId, type): void {
    let input;
    if (type === 'child') {
      input = $(`#cInput_${commentId}`)[0];
    } else {
      input = $(`#pInput_${commentId}`)[0];
    }

    const body = {
      description: input.value,
    };

    this.posterService.updateComment(this.posterId, commentId, body).subscribe(res => {
      this.getPosterComments();
    });
  }

  isModify(inputId): boolean {
    if ($(`#${inputId}`) && $(`#${inputId}`)[0]) {
      return !$(`#${inputId}`)[0].disabled;
    }
    return false;
  }

  cancel(): void {
    this.getPosterComments();
  }

  /** 댓글을 삭제한다 */
  remove(commentId): void {
    if (confirm('댓글을 삭제하시겠습니까?')) {
      this.posterService.deleteComment(this.posterId, commentId).subscribe(res => {
        this.getPosterComments();
      });
    }
  }

  @HostListener('contextmenu', ['$event'])
  onRightClick(event): void {
    event.preventDefault();
  }
}

