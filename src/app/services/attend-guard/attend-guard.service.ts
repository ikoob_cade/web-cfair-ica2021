import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, NavigationCancel } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HistoryService } from '../api/history.service';
import { MemberService } from '../api/member.service';
import { DataService } from '../data.service';

@Injectable({
  providedIn: 'root'
})
export class AttendGuard implements CanActivate {
  curLang;
  msgText;

  constructor(
    private router: Router,
    private historyService: HistoryService,
    private memberService: MemberService,
    private translateService: TranslateService,
    private dataService: DataService
  ) {
    this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
        setTimeout(() => {
          this.translateService.get('msg-exit').subscribe(res => {
            this.msgText = res;
          });
        }, 0);
      });
  }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    // 라이브에서 퇴실 상황인 경우
    if (!this.historyService.isAttended()) {
      if (confirm(this.msgText)) {
        const options: { relationId: string, relationType: string, logType: string } = {
          relationId: sessionStorage.getItem('currentRoom'),
          relationType: 'room',
          logType: this.historyService.getAttendance()
        };

        const user = JSON.parse(sessionStorage.getItem('cfair'));
        // 서버에 저장
        this.memberService.history(
          user.id,
          options).subscribe(res => {
            sessionStorage.removeItem('currentRoom');
          });

        this.historyService.setAttendance(null);

        // this.router.navigate([next.routeConfig.path]);
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

}
