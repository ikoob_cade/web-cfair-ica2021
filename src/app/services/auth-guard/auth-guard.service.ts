import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MemberService } from '../api/member.service';
import { DayjsService } from '../dayjs.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private dayjsService: DayjsService,
    private memberService: MemberService,
  ) { }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const auth = JSON.parse(sessionStorage.getItem('cfair'));
    if (auth && auth.token) {
      if (auth.isStamp) {
        const isBefore = await this.memberService.compareTime('2021-12-01 06:00:00').toPromise();
        // const isBefore = await this.memberService.compareTime('2021-11-30 16:28:10').toPromise();

        if (!isBefore) {
          const path = ['live', 'vod', 'vod/:vodId'];
          const result = path.indexOf(route.routeConfig.path);

          if (result > -1) {
            alert('You are available to access to a pre-congress only.');
            this.router.navigate(['/main']);
          }
        }
      }

      if (route.routeConfig.path === 'login') {
        this.router.navigate(['/main']);
      }
      return true;
    } else {
      // 아래 path는 로그인 상태에서만 접근 가능
      const path = ['live', 'vod', 'vod/:vodId', 'documents', 'document-write', 'my-page'];

      const result = path.indexOf(route.routeConfig.path);
      if (result > -1) {
        alert('Login is required.');
        this.router.navigate(['/login']);
      }
      return true;

    }
  }
}

