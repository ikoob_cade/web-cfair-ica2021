import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {
    private isReceiveMessage: BehaviorSubject<any> = new BehaviorSubject(null);
    currentReceiveMessage = this.isReceiveMessage.asObservable();

    private isForceLogout: BehaviorSubject<any> = new BehaviorSubject(null);
    forceLogout = this.isForceLogout.asObservable();

    private languageManager: BehaviorSubject<any> = new BehaviorSubject('en');
    currentLanguage = this.languageManager.asObservable();

    constructor() {
        this.changeCurrentLanguage(localStorage.getItem('lang') ?? 'en');
    }

    changeReceiveMessage(data: any): void {
        this.isReceiveMessage.next(data);
    }

    doForceLogout(data: any): void {
        this.isForceLogout.next(data);
    }

    changeCurrentLanguage(lang: string): void {
        localStorage.setItem('lang', lang);
        this.languageManager.next(lang);
    }

}
