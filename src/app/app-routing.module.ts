import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Service
import { AuthGuard } from './services/auth-guard/auth-guard.service';
import { AttendGuard } from './services/attend-guard/attend-guard.service';

// Page
import { MainComponent } from './pages/main/main.component';
import { AboutComponent } from './pages/about/about.component';
import { LoginComponent } from './pages/login/login.component';
import { SpeakersComponent } from './pages/speakers/speakers.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RegistraionComponent } from './pages/registraion/registraion.component';
import { MyPageComponent } from './pages/my-page/my-page.component';
import { ProgramComponent } from './pages/program/program.component';
import { LiveComponent } from './pages/program/live/live.component';
import { VodComponent } from './pages/program/vod/vod.component';
import { VodDetailComponent } from './pages/program/vod-detail/vod-detail.component';
import { AgendaInfoComponent } from './pages/program/agenda-info/agenda-info.component';
import { BoardComponent } from './pages/board/board.component';
import { BoardDetailComponent } from './pages/board/board-detail/board-detail.component';
import { BoardWriteComponent } from './pages/board/board-write/board-write.component';
import { NotSupportIeComponent } from './pages/not-support-ie/not-support-ie.component';
import { EBoothComponent } from './pages/e-booth/e-booth.component';
import { DocumentComponent } from './pages/documents/document.component';
import { DocumentWriteComponent } from './pages/documents/document-write/document-write.component';

const routes: Routes = [
  { path: 'main', component: MainComponent, canActivate: [AttendGuard] },
  { path: 'about', component: AboutComponent, canActivate: [AttendGuard] },
  { path: 'program', component: ProgramComponent, canActivate: [AttendGuard] },

  // { path: 'live', component: LiveComponent, canActivate: [AuthGuard, AttendGuard] },
  // { path: 'live/:dateId/:roomId/:agendaId', component: AgendaInfoComponent, canActivate: [AuthGuard, AttendGuard] },

  { path: 'vod', component: VodComponent, canActivate: [AuthGuard, AttendGuard] },
  { path: 'vod/:agendaId', component: VodDetailComponent, canActivate: [AuthGuard, AttendGuard] },

  { path: 'speakers', component: SpeakersComponent, canActivate: [AttendGuard] },

  { path: 'documents', component: DocumentComponent, canActivate: [AuthGuard, AttendGuard] },
  { path: 'document-write', component: DocumentWriteComponent, canActivate: [AuthGuard, AttendGuard] },
  // { path: 'posters/:posterId', component: PostersDetailComponent, canActivate: [AuthGuard, AttendGuard] },

  { path: 'e-booth', component: EBoothComponent, canActivate: [AttendGuard] },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard, AttendGuard] },

  { path: 'my-page', component: MyPageComponent, canActivate: [AttendGuard, AuthGuard] },
  { path: 'registration', component: RegistraionComponent },

  { path: 'board', component: BoardComponent, canActivate: [AttendGuard] },
  { path: 'board/:boardId', component: BoardDetailComponent, },
  { path: 'board-regist', component: BoardWriteComponent, canActivate: [AttendGuard] },
  { path: 'not-ie', component: NotSupportIeComponent },
  { path: '', redirectTo: '/main', pathMatch: 'full', canActivate: [AttendGuard] },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
    RouterModule.forRoot(routes, { enableTracing: false, onSameUrlNavigation: 'reload' }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
