import { Component, OnInit, AfterViewInit, ChangeDetectorRef, Input, ViewChild, OnDestroy, ViewChildren, QueryList, ComponentFactoryResolver } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
import { MemberService } from '../../services/api/member.service';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';

declare let $: any;

/**
 * TODO
 * ICA용 youtube URL.
 * 공유 - Embed코드 사용 -> iframe 생성
 */
@Component({
  selector: 'app-player-youtube',
  templateUrl: './player-youtube.component.html',
  styleUrls: ['./player-youtube.component.scss']
})
export class PlayerYoutubeComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input('youtubeUrl') youtubeUrl: any; // 전달받은 유튜브 URL
  @Input('relationId') relationId: any; // roomId or vodId
  @Input('curLang') curLang: any;
  @Input('isLive') isLive: any;

  public user: any;
  public viewer: any;
  public liveUrl;

  @ViewChild('commentList') commentList: any; // 댓글 목록

  public replyForm: FormGroup;
  public replys: any = [];
  private timerID;
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보


  // TODO 임시데이타
  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    private sanitizer: DomSanitizer
  ) {
    this.replyForm = fb.group({
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    if (this.isLive) {
      this.getComment();
      this.timerID = setInterval(() => {
        this.getComment();
      }, 10 * 1000);
    }

    this.liveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.youtubeUrl);
  }

  ngOnDestroy(): void {
    clearInterval(this.timerID);
  }

  ngAfterViewInit(): void {
    // this.things.changes.subscribe(t => {
    //   this.ngForRendered();
    // })
    // this.cdr.detectChanges();
  }

  /** 댓글달기 */
  comment(): void {
    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content,
      relationId: this.relationId
    }).subscribe(res => {
      this.getComment();
      // this.replyForm.value.content = '';
      this.replyForm.patchValue({
        content: ''
      });
      if (this.isLive) {
        this.downScroll();
      }
    });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop = this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    if (confirm('Are you sure you want to delete?')) {
      this.memberService.deleteComment(this.user.id, comment.id)
        .subscribe(res => {
          this.getComment();
        });
    }
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService.findComment(this.user.id, this.relationId).subscribe(res => {
      let isBottom = false;
      if (this.commentList.nativeElement.scrollTop === (this.commentList.nativeElement.scrollHeight - this.commentList.nativeElement.offsetHeight)) {
        isBottom = true;
      }
      this.replys = res;
      setTimeout(() => {
        if (isBottom) {
          this.downScroll();
        }
      }, 800);
    });
  }
}
