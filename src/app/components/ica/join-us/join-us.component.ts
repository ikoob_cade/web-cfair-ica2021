import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-join-us',
  templateUrl: './join-us.component.html',
  styleUrls: ['./join-us.component.scss']
})
export class JoinUsComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  scrollToTable(): void {
    const tableHeader = document.getElementById('scroll-destination');
    tableHeader.scrollIntoView(true);
  }

}
