import * as _ from 'lodash';
import {
  Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef,
  AfterContentChecked, OnDestroy, AfterViewInit
} from '@angular/core';
import { forkJoin as observableForkJoin, Observable, Subscription } from 'rxjs';
import { DateService } from '../../services/api/date.service';
import { RoomService } from '../../services/api/room.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DayjsService } from '../../services/dayjs.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AgendaService } from '../../services/api/agenda.service';
import { DataService } from '../../services/data.service';
import { MemberService } from '../../services/api/member.service';
declare let $: any;
@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit, AfterContentChecked, OnDestroy, AfterViewInit {
  @ViewChild('sessionDetailBtn') sessionDetailBtn: ElementRef;
  @ViewChild('table1', { read: ElementRef }) table1: ElementRef;
  @ViewChild('th1', { read: ElementRef }) th1: ElementRef;
  @ViewChild('th2', { read: ElementRef }) th2: ElementRef;
  @ViewChild('th3', { read: ElementRef }) th3: ElementRef;
  // tslint:disable-next-line: no-input-rename
  @Input('agendas') agendas: any;

  public mainDate = 3;
  public preDate = 3;

  private koreaTimer;
  public koreaTime = this.dayjsService.getNow().format('YYYY - MM - DD HH:mm:ss');

  private user = sessionStorage.getItem('cfair') ? JSON.parse(sessionStorage.getItem('cfair')) : null;
  public selectedAgenda;
  public selectedSubList;

  private dates: any;
  private rooms: any;

  public curLang;
  private subscriptions = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private dateService: DateService,
    private roomService: RoomService,
    private dayjsService: DayjsService,
    private deviceService: DeviceDetectorService,
    private agendaService: AgendaService,
    private dataService: DataService,
    private memberService: MemberService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  ngOnInit(): void {
    this.doInit();
    this.getTime();
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  }

  setMainDate = (index: number) => {
    this.mainDate = index;
  }

  setPreDate = (index: number) => {
    this.preDate = index;
  }

  goLiveV2(dateId, roomId): void {
    const findDate = _.find(this.dates, date => date.id === dateId);

    if (findDate && (findDate.date === '2021-12-02' || findDate.date === '2021-12-03')) {
      this.router.navigate(['/live'], { queryParams: { dateId, roomId } });
    } else {

      const findRoom = _.find(this.rooms, room => room.id === roomId);
      console.log(findRoom);

      this.router.navigate(['/vod']);
    }
  }

  ngAfterViewInit(): void {
    this.activatedRoute.queryParams.subscribe(res => {
      const device = this.deviceService.getDeviceInfo();
      if (res.focusTo && res.focusTo === 'pre') {
        if (device.browser !== 'Safari') {
          const preTable: HTMLElement = document.getElementById('PreTable');
          preTable.scrollIntoView({ block: 'center' });
        } else {
          setTimeout(() => {
            window.scrollTo(0, screen.height / 1);
          }, 500);
        }
      }
    });
  }

  doInit(): void {
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .subscribe(resp => {
        this.dates = resp[0];
        this.rooms = resp[1];
      });
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * Live 상세보기
   * @param date 순서
   * @param room 순서
   */
  goLive(date: number, room: number): void {
    this.router.navigate(['/live'], { queryParams: { dateId: this.dates[date].id, roomId: this.rooms[room].id } });
  }

  /**
   * 줌채널로 바로가기
   */
  goLiveZoom(room: number): void {
    this.router.navigate(['/live'], { queryParams: { dateId: this.dates[this.preDate - 1].id, roomId: this.rooms[room].id } });
  }

  goPoster(): void {
    this.router.navigate(['/posters']);
  }

  goVod(): void {
    this.router.navigate(['/vod'], {
      queryParams: {
        dateId: this.selectedAgenda.dateId,
        roomId: this.selectedAgenda.roomId
      }
    });
  }

  public scrollToTable(): void {
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = this.dayjsService.getNow().format(' - YYYY - MM - DD / HH : mm : ss');
    }, 1000);
  }

  getSessionDetail(sessionId: string): void {
    if (this.user) {
      // 초기화
      this.selectedAgenda = null;
      this.selectedSubList = null;

      this.agendaService.findSessionSublist(sessionId).subscribe(res => {
        this.sessionDetailBtn.nativeElement.click();
        setTimeout(() => {
          this.selectedAgenda = res.agenda; // 클릭한 부모세션
          this.selectedSubList = res.subList; // 하위 강의 목록
        }, 500);
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }


}
