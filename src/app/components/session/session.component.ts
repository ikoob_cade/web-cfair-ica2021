import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line: no-input-rename
  @Input('session') sessionData: any; // 세션 정보
  // tslint:disable-next-line: no-output-rename
  @Output('detailFn') detailFn = new EventEmitter(); // 자세히보기
  // tslint:disable-next-line: no-input-rename
  @Input('curLang') curLang: any;

  public session;
  public polling: any;
  myStroage = sessionStorage;
  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.session = this.sessionData;
  }

  ngOnDestroy(): void {
    clearTimeout(this.polling);
  }

}
