import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit, OnDestroy {
  @Input('agenda') agendaData: any; // 아젠다 정보
  @Output('detailFn') detailFn = new EventEmitter(); // 자세히보기

  // tslint:disable-next-line: no-input-rename
  @Input('curLang') curLang: any;

  agenda;

  public isLive: boolean = false;
  public polling: any;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.agenda = this.agendaData;

    this.checkLive();
  }

  ngOnDestroy(): void {
    clearTimeout(this.polling);
  }

  // Check Live
  checkLive(): void {
    const agendaDate = this.agenda.date.date.replace(/-/gi, '/');
    const agendaStartTime = new Date(`${agendaDate}/${this.agenda.startTime}`);
    const agendaEndTime = new Date(`${agendaDate}/${this.agenda.endTime}`);
    const now: Date = new Date();
    if (now > agendaStartTime && now < agendaEndTime) {
      this.isLive = true;
    } else {
      this.isLive = false;
    }
    const intervalNum = 60; // TODO 1분 간격 갱신
    this.polling = setTimeout(() => { this.checkLive(); }, 1000 * intervalNum);
  }

}
