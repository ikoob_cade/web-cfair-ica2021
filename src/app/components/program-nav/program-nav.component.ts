import * as _ from 'lodash';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-program-nav',
  templateUrl: './program-nav.component.html',
  styleUrls: ['./program-nav.component.scss']
})
export class ProgramNavComponent implements OnInit {
  @Output('setDateFn') setDateFn = new EventEmitter();
  @Output('setRoomFn') setRoomFn = new EventEmitter();

  @Input('selected') selected: any;

  @Input('dates') dates: any;
  @Input('rooms') rooms: any;
  @Input('isVodDetail') isVodDetail: any;

  constructor() { }

  ngOnInit(): void {
  }
}
