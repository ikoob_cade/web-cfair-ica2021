import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';

@Component({
  selector: 'app-speaker',
  templateUrl: './speaker.component.html',
  styleUrls: ['./speaker.component.scss']
})
export class SpeakerComponent implements OnInit {
  @Input('speaker') speakerData: any;
  @Output('speakerDetailFn') detailFn = new EventEmitter(); // 자세히보기
  @Input('curLang') curLang: any;

  speaker;

  constructor(
    public speakerService: SpeakerService,
  ) {
  }

  ngOnInit(): void {
    this.speaker = this.speakerData;
  }
}
