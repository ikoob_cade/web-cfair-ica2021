import * as _ from 'lodash';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-program-live',
  templateUrl: './program-live.component.html',
  styleUrls: ['./program-live.component.scss']
})

export class ProgramLiveComponent implements OnInit, OnDestroy {
  // tslint:disable-next-line:no-input-rename
  @Input('program-live') programLive;
  // tslint:disable-next-line:no-input-rename
  @Input('speakerId') speakerId;

  public curLang;
  private subscriptions = [];

  constructor(
    private router: Router,
    private dataService: DataService
  ) {
    const languageSubscription = this.dataService.currentLanguage
      .subscribe((data) => {
        this.curLang = data;
      });

    this.subscriptions.push(languageSubscription);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    _.forEach(this.subscriptions, (subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

  /**
   * 라이브로 이동한다.
   * @param agenda 아젠다
   */
  goLive(agenda): void {
    this.router.navigate(['/live'], { queryParams: { dateId: agenda.dateId, roomId: agenda.roomId } });
  }
}
