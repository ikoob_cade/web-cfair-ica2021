import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from '../../services/api/member.service';
import { AuthService } from '../../services/auth/auth.service';
import { EventService } from '../../services/api/event.service';
import { SocketService } from '../../services/socket/socket.service';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from '../../services/data.service';
import { HistoryService } from '../../../app/services/api/history.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output('showNetwork') showNetworkFn: EventEmitter<any> = new EventEmitter();
  @Output('hideNetwork') hideNetworkFn: EventEmitter<any> = new EventEmitter();

  public event;
  public user: any;
  public menus: any = [];
  public collapse = true;

  constructor(
    private router: Router,
    private eventService: EventService,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
    private translateService: TranslateService,
    private dataService: DataService,
    private historyService: HistoryService
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(sessionStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;

        scrollTo({ top: 0 });

        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;
      });

    this.setMenuText();
  }

  ngOnInit(): void { }

  // 버전 확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(this.event.eventCode + 'ver');
        if (!clientVersion) {
          localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
            location.reload();
          }
        }
      }
    });
  }

  // 유저데이터 제거
  private removeUser(): void {
    this.logoutSocket();
    sessionStorage.removeItem('cfair');
    this.user = null;
  }

  // logout
  logout(): void {
    this.hideNetworkFn.emit(true);
    const currentRoom = this.historyService.getRoom();
    const attendance = this.historyService.getAttendance();

    // 퇴장을 남겨야 할 경우만 동작
    if (currentRoom && attendance === 'out') {
      const options: { relationId: string, relationType: string, logType: string } = {
        relationId: currentRoom.id,
        relationType: 'room',
        logType: this.historyService.getAttendance()
      };

      this.memberService.history(this.user.id, options)
        .subscribe(() => {
          this.historyService.setAttendance('in');
          this.router.navigate(['/main']).finally(() => {
            this.removeUser();
          });
        },
          (error) => {
            console.error(error);
            this.router.navigate(['/main'])
              .catch(() => {
                this.removeUser();
              })
              .finally(() => {
                this.removeUser();
              });
          });
    } else {
      this.router.navigate(['/main'])
        .catch(error => {
          this.removeUser();
        })
        .finally(() => {
          this.removeUser();
        });
    }
  }

  // socket loggin
  loginSocket(): void {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token
    });
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
  }

  // socket logout
  logoutSocket(): void {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  toggleMenu(): void {
    if (this.collapse) {
      this.showNetworkFn.emit(false);
    }
    this.collapse = !this.collapse;
  }

  setMenuText(): void {
    this.translateService.use(localStorage.getItem('lang') ?? 'en').subscribe((res) => {
      this.menus = [
        { title: res.header.welcome, router: '/about' },
        { title: res.header.program, router: '/program' },
        // { title: res.header.live, router: '/live', isLive: true },
        { title: res.header.vod, router: '/vod', isVod: true },
        { title: res.header.speakers, router: '/speakers' },
        { title: res.header.sponsors, router: '/e-booth' },
        { title: res.header.notice, router: '/board' },
        { title: res.header.document, router: '/documents' },
      ];
    });
  }

  /**
   *  현재 언어를 변경한다.
   * @param lang en / es / fr / kr
   */
  changeLang(lang): void {
    this.dataService.changeCurrentLanguage(lang);
    this.setMenuText();

    $('.navbar-collapse').collapse('hide');
    this.collapse = true;
  }

  // ! app.component.ts
  showNetwork(): void {
    this.showNetworkFn.emit(true);
    $('.navbar-collapse').collapse('hide');
    this.collapse = true;
  }
}
